package main
import (
    "fmt"
)

func main() {
    var s int
    sum(&s, 1,2,3)
    fmt.Println("s=", s)
}

func sum(r *int, x... int) int {
    for _, v := range x {
        *r += v
    }
    return *r
}
