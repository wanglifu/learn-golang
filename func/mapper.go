package main
import (
    "fmt"
)

type S interface{}

type Student struct {
    name string
    addr string
}
func f(x S) (name string) {
    s,_ := x.(Student)
    return s.name
}
func mapper(list []S, f func(S) string) {
    s := list[len(list)-1]
    fmt.Println(f(s))
}

func main() {
    a := Student{"A", "b"}
    group := []S{
        Student{"Ken Pu", "Oshawa"},
        Student{"Smith", "Toronto"},
        a}
    fmt.Println(f(group[0]))
    mapper(group, f)
}
