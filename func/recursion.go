package main
import (
    "fmt"
)

func looper(i int) {
    looper(i+1)
    looper(i+1)
}

func main() {
    defer func() {
        if r := recover(); r != nil {
            fmt.Printf("======= ENDED AT ===========\n", i)
        }
    }()
    looper(0)
}
