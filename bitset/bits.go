package main
import (
    "fmt"
)

type Bits uint64
func (bits *Bits) Set(i uint) *Bits {
    *bits = *bits | (1 << i)
    return bits
}
func (bits *Bits) Print() {
    var i uint
    for i=0; i < 64; i++ {
        bit := ((*bits & (1 << i)) != 0)
        if bit {
            fmt.Print(1)
        } else {
            fmt.Print(0)
        }
    }
    fmt.Print("\n")
}

func main() {
    var bits *Bits
    bits = new(Bits)
    bits.Set(16).Set(17).Set(18)
    bits.Print()
}
